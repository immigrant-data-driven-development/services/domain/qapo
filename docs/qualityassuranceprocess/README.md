# 📕Documentation: QualityAssuranceProcess



## 🌀 Package's Data Model

![Domain Diagram](classdiagram.png)

### ⚡Entities

* **QualityAssuranceProcess** : is a Specific Performed Process for conducting the activities
related to software quality assurance, assessing and assuring adherence of the performed
processes and produced artifacts to the applicable requirements
* **QualityAssurancePlanning** : -
* **QualityAssurancePlan** : -
* **QualityAssuranceCriterion** : -
* **AdherenceEvaluation** : Adherence Evaluation activities for objectively evaluating the
adherence of processes and products to the applicable requirements, registering the identified
issues, creating an Evaluation Report.
* **EvaluationReport** : An Evaluation Report is a Document describing the
evaluation results and identified issues
* **EvaluatedItem** : -
* **ArtifactEvaluation** : An Artifact Evaluation is an activity for
objectively evaluating the adherence of products and deliverables.
* **EvaluatedArtifact** : -
* **NonComplianceIdentification** : is an activity for registering the
noncompliances identified in processes and artifacts in a Noncompliance Register
* **NonComplianceRegister** : A Noncompliance Register (e.g., a neglected activity in a process, a document wrongly specified) is an
Information Item describing a noncompliance (a failure or refusal to conform to an applicable
requirement) in a process or artifact, and related information to solve it.
