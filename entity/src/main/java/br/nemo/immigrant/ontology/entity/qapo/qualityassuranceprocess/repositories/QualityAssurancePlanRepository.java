package br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.QualityAssurancePlan;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface QualityAssurancePlanRepository extends PagingAndSortingRepository<QualityAssurancePlan, Long>, ListCrudRepository<QualityAssurancePlan, Long> {

}
