package br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;



import br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "evaluatedartifact")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class EvaluatedArtifact extends Artifact implements Serializable {




  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "evaluatedartifact")
  @Builder.Default
  Set<EvaluatedItem> evaluateditems = new HashSet<>();

  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "evaluatedartifact")
  @Builder.Default
  Set<EvaluationReport> evaluationreports = new HashSet<>();





  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        EvaluatedArtifact elem = (EvaluatedArtifact) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "EvaluatedArtifact {" +
         "id="+this.id+


      '}';
  }
}
