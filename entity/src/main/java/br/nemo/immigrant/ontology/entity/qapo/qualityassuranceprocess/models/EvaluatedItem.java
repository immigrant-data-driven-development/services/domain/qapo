package br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;



import br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "evaluateditem")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class EvaluatedItem extends Artifact implements Serializable {




  @ManyToOne
  @JoinColumn(name = "evaluationreport_id")
  private EvaluationReport evaluationreport;

  @ManyToMany
  @JoinTable(
      name = "evaluateditem_qualityassurancecriterion",
      joinColumns = @JoinColumn(name = "evaluateditem_id"),
      inverseJoinColumns = @JoinColumn(name = "qualityassurancecriterion_id")
  )
  @Builder.Default
  private Set<QualityAssuranceCriterion> qualityassurancecriterions = new HashSet<>();

  @ManyToOne
  @JoinColumn(name = "evaluatedartifact_id")
  private EvaluatedArtifact evaluatedartifact;





  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        EvaluatedItem elem = (EvaluatedItem) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "EvaluatedItem {" +
         "id="+this.id+


      '}';
  }
}
