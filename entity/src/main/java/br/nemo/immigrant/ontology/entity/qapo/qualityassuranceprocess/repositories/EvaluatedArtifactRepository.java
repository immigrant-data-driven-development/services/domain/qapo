package br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.EvaluatedArtifact;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface EvaluatedArtifactRepository extends PagingAndSortingRepository<EvaluatedArtifact, Long>, ListCrudRepository<EvaluatedArtifact, Long> {

}
