package br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;



import br.nemo.immigrant.ontology.entity.rsro.requirements.models.Requirement;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "qualityassurancecriterion")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class QualityAssuranceCriterion extends Requirement implements Serializable {




  @ManyToMany(mappedBy = "qualityassurancecriterions")
  @Builder.Default
  private Set<QualityAssurancePlan> qualityassuranceplans = new HashSet<>();

  @ManyToMany(mappedBy = "qualityassurancecriterions")
  @Builder.Default
  private Set<EvaluatedItem> evaluateditems = new HashSet<>();





  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        QualityAssuranceCriterion elem = (QualityAssuranceCriterion) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "QualityAssuranceCriterion {" +
         "id="+this.id+


      '}';
  }
}
