package br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.NonComplianceIdentification;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface NonComplianceIdentificationRepository extends PagingAndSortingRepository<NonComplianceIdentification, Long>, ListCrudRepository<NonComplianceIdentification, Long> {

}
