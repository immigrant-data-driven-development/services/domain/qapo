package br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.QualityAssurancePlanning;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface QualityAssurancePlanningRepository extends PagingAndSortingRepository<QualityAssurancePlanning, Long>, ListCrudRepository<QualityAssurancePlanning, Long> {

}
