package br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;



import br.nemo.immigrant.ontology.entity.spo.process.models.Activity;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "adherenceevaluation")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class AdherenceEvaluation extends Activity implements Serializable {




  @ManyToMany
  @JoinTable(
      name = "adherenceevaluation_evaluationreport",
      joinColumns = @JoinColumn(name = "adherenceevaluation_id"),
      inverseJoinColumns = @JoinColumn(name = "evaluationreport_id")
  )
  @Builder.Default
  private Set<EvaluationReport> evaluationreports = new HashSet<>();
/*
  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "adherenceevaluation")
  @Builder.Default
  Set<ArtifactEvaluation> artifactevaluations = new HashSet<>();

  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "adherenceevaluation")
  @Builder.Default
  Set<NonComplianceIdentification> noncomplianceidentifications = new HashSet<>();
*/
  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        AdherenceEvaluation elem = (AdherenceEvaluation) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "AdherenceEvaluation {" +
         "id="+this.id+


      '}';
  }
}
