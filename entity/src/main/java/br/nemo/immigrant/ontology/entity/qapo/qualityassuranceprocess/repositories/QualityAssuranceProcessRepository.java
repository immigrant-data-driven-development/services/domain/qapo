package br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.QualityAssuranceProcess;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface QualityAssuranceProcessRepository extends PagingAndSortingRepository<QualityAssuranceProcess, Long>, ListCrudRepository<QualityAssuranceProcess, Long> {

}
