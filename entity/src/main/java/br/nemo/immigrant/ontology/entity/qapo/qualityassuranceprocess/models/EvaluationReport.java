package br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;



import br.nemo.immigrant.ontology.entity.spo.artifact.models.Artifact;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "evaluationreport")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class EvaluationReport extends Artifact implements Serializable {




  @ManyToMany(mappedBy = "evaluationreports")
  @Builder.Default
  private Set<AdherenceEvaluation> adherenceevaluations = new HashSet<>();
/*
  @OneToMany(cascade = {CascadeType.ALL}, orphanRemoval = true, mappedBy = "evaluationreport")
  @Builder.Default
  Set<EvaluatedItem> evaluateditems = new HashSet<>();
*/
  @ManyToOne
  @JoinColumn(name = "evaluatedartifact_id")
  private EvaluatedArtifact evaluatedartifact;





  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        EvaluationReport elem = (EvaluationReport) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "EvaluationReport {" +
         "id="+this.id+


      '}';
  }
}
