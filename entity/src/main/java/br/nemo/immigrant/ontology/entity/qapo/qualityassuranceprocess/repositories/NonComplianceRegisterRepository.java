package br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.NonComplianceRegister;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface NonComplianceRegisterRepository extends PagingAndSortingRepository<NonComplianceRegister, Long>, ListCrudRepository<NonComplianceRegister, Long> {

}
