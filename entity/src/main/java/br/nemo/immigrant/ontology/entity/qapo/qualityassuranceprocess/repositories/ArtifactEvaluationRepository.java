package br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.ArtifactEvaluation;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface ArtifactEvaluationRepository extends PagingAndSortingRepository<ArtifactEvaluation, Long>, ListCrudRepository<ArtifactEvaluation, Long> {

}
