package br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models;

import lombok.Data;
import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;
import lombok.experimental.SuperBuilder;

import jakarta.persistence.*;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.HashSet;
import java.util.Objects;
import java.util.UUID;
import java.time.LocalDate;
import java.util.Date;



import br.nemo.immigrant.ontology.entity.spo.process.models.Activity;

@Data
@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
//@Table(name = "noncomplianceidentification")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public  class NonComplianceIdentification extends Activity implements Serializable {



/*
  @ManyToOne
  @JoinColumn(name = "adherenceevaluation_id")
  private AdherenceEvaluation adherenceevaluation;
*/
  @ManyToMany
  @JoinTable(
      name = "noncomplianceidentification_noncomplianceregister",
      joinColumns = @JoinColumn(name = "noncomplianceidentification_id"),
      inverseJoinColumns = @JoinColumn(name = "noncomplianceregister_id")
  )
  @Builder.Default
  private Set<NonComplianceRegister> noncomplianceregisters = new HashSet<>();





  @Builder.Default
  private LocalDateTime createdAt = LocalDateTime.now();

  @Override
  public boolean equals(Object o) {
          if (this == o) return true;
          if (o == null || this.getClass() != o.getClass()) return false;

        NonComplianceIdentification elem = (NonComplianceIdentification) o;
        return getId().equals(elem.getId());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getId());
  }

  @Override
  public String toString() {
      return "NonComplianceIdentification {" +
         "id="+this.id+


      '}';
  }
}
