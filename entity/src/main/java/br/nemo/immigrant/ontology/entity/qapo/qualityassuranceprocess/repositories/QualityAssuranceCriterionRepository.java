package br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.QualityAssuranceCriterion;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface QualityAssuranceCriterionRepository extends PagingAndSortingRepository<QualityAssuranceCriterion, Long>, ListCrudRepository<QualityAssuranceCriterion, Long> {

}
