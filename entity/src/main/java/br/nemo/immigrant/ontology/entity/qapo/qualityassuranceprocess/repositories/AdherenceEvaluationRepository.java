package br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.AdherenceEvaluation;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface AdherenceEvaluationRepository extends PagingAndSortingRepository<AdherenceEvaluation, Long>, ListCrudRepository<AdherenceEvaluation, Long> {

}
