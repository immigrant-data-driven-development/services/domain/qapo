package br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.EvaluationReport;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface EvaluationReportRepository extends PagingAndSortingRepository<EvaluationReport, Long>, ListCrudRepository<EvaluationReport, Long> {

}
