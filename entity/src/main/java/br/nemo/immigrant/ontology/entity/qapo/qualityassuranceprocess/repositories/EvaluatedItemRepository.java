package br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.EvaluatedItem;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.ListCrudRepository;

public interface EvaluatedItemRepository extends PagingAndSortingRepository<EvaluatedItem, Long>, ListCrudRepository<EvaluatedItem, Long> {

}
