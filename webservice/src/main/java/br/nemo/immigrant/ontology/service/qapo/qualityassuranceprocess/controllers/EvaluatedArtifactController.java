package br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.controllers;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.EvaluatedArtifact;
import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories.EvaluatedArtifactRepository;
import br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.records.EvaluatedArtifactInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class EvaluatedArtifactController  {

  @Autowired
  EvaluatedArtifactRepository repository;

  @QueryMapping
  public List<EvaluatedArtifact> findAllEvaluatedArtifacts() {
    return repository.findAll();
  }

  @QueryMapping
  public EvaluatedArtifact findByIDEvaluatedArtifact(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public EvaluatedArtifact createEvaluatedArtifact(@Argument EvaluatedArtifactInput input) {
    EvaluatedArtifact instance = EvaluatedArtifact.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public EvaluatedArtifact updateEvaluatedArtifact(@Argument Long id, @Argument EvaluatedArtifactInput input) {
    EvaluatedArtifact instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("EvaluatedArtifact not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteEvaluatedArtifact(@Argument Long id) {
    repository.deleteById(id);
  }

}
