package br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.repositories;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.ArtifactEvaluation;
import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories.ArtifactEvaluationRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "artifactevaluation", path = "artifactevaluation")
public interface ArtifactEvaluationRepositoryWeb extends ArtifactEvaluationRepository {

}
