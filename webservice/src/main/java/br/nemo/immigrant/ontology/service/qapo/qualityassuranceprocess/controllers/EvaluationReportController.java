package br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.controllers;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.EvaluationReport;
import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories.EvaluationReportRepository;
import br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.records.EvaluationReportInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class EvaluationReportController  {

  @Autowired
  EvaluationReportRepository repository;

  @QueryMapping
  public List<EvaluationReport> findAllEvaluationReports() {
    return repository.findAll();
  }

  @QueryMapping
  public EvaluationReport findByIDEvaluationReport(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public EvaluationReport createEvaluationReport(@Argument EvaluationReportInput input) {
    EvaluationReport instance = EvaluationReport.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public EvaluationReport updateEvaluationReport(@Argument Long id, @Argument EvaluationReportInput input) {
    EvaluationReport instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("EvaluationReport not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteEvaluationReport(@Argument Long id) {
    repository.deleteById(id);
  }

}
