package br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.repositories;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.AdherenceEvaluation;
import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories.AdherenceEvaluationRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "adherenceevaluation", path = "adherenceevaluation")
public interface AdherenceEvaluationRepositoryWeb extends AdherenceEvaluationRepository {

}
