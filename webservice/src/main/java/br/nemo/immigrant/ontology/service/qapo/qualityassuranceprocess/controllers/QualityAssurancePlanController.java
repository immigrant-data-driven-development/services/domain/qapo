package br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.controllers;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.QualityAssurancePlan;
import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories.QualityAssurancePlanRepository;
import br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.records.QualityAssurancePlanInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class QualityAssurancePlanController  {

  @Autowired
  QualityAssurancePlanRepository repository;

  @QueryMapping
  public List<QualityAssurancePlan> findAllQualityAssurancePlans() {
    return repository.findAll();
  }

  @QueryMapping
  public QualityAssurancePlan findByIDQualityAssurancePlan(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public QualityAssurancePlan createQualityAssurancePlan(@Argument QualityAssurancePlanInput input) {
    QualityAssurancePlan instance = QualityAssurancePlan.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public QualityAssurancePlan updateQualityAssurancePlan(@Argument Long id, @Argument QualityAssurancePlanInput input) {
    QualityAssurancePlan instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("QualityAssurancePlan not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteQualityAssurancePlan(@Argument Long id) {
    repository.deleteById(id);
  }

}
