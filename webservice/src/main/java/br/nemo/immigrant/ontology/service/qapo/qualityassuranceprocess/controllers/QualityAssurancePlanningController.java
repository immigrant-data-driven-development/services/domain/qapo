package br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.controllers;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.QualityAssurancePlanning;
import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories.QualityAssurancePlanningRepository;
import br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.records.QualityAssurancePlanningInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class QualityAssurancePlanningController  {

  @Autowired
  QualityAssurancePlanningRepository repository;

  @QueryMapping
  public List<QualityAssurancePlanning> findAllQualityAssurancePlannings() {
    return repository.findAll();
  }

  @QueryMapping
  public QualityAssurancePlanning findByIDQualityAssurancePlanning(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public QualityAssurancePlanning createQualityAssurancePlanning(@Argument QualityAssurancePlanningInput input) {
    QualityAssurancePlanning instance = QualityAssurancePlanning.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public QualityAssurancePlanning updateQualityAssurancePlanning(@Argument Long id, @Argument QualityAssurancePlanningInput input) {
    QualityAssurancePlanning instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("QualityAssurancePlanning not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteQualityAssurancePlanning(@Argument Long id) {
    repository.deleteById(id);
  }

}
