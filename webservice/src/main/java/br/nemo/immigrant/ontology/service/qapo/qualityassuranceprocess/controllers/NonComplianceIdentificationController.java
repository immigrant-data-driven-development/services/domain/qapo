package br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.controllers;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.NonComplianceIdentification;
import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories.NonComplianceIdentificationRepository;
import br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.records.NonComplianceIdentificationInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class NonComplianceIdentificationController  {

  @Autowired
  NonComplianceIdentificationRepository repository;

  @QueryMapping
  public List<NonComplianceIdentification> findAllNonComplianceIdentifications() {
    return repository.findAll();
  }

  @QueryMapping
  public NonComplianceIdentification findByIDNonComplianceIdentification(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public NonComplianceIdentification createNonComplianceIdentification(@Argument NonComplianceIdentificationInput input) {
    NonComplianceIdentification instance = NonComplianceIdentification.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public NonComplianceIdentification updateNonComplianceIdentification(@Argument Long id, @Argument NonComplianceIdentificationInput input) {
    NonComplianceIdentification instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("NonComplianceIdentification not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteNonComplianceIdentification(@Argument Long id) {
    repository.deleteById(id);
  }

}
