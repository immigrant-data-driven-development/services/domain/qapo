package br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.repositories;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.NonComplianceIdentification;
import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories.NonComplianceIdentificationRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "noncomplianceidentification", path = "noncomplianceidentification")
public interface NonComplianceIdentificationRepositoryWeb extends NonComplianceIdentificationRepository {

}
