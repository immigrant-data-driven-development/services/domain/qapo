package br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.controllers;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.ArtifactEvaluation;
import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories.ArtifactEvaluationRepository;
import br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.records.ArtifactEvaluationInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class ArtifactEvaluationController  {

  @Autowired
  ArtifactEvaluationRepository repository;

  @QueryMapping
  public List<ArtifactEvaluation> findAllArtifactEvaluations() {
    return repository.findAll();
  }

  @QueryMapping
  public ArtifactEvaluation findByIDArtifactEvaluation(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public ArtifactEvaluation createArtifactEvaluation(@Argument ArtifactEvaluationInput input) {
    ArtifactEvaluation instance = ArtifactEvaluation.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public ArtifactEvaluation updateArtifactEvaluation(@Argument Long id, @Argument ArtifactEvaluationInput input) {
    ArtifactEvaluation instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("ArtifactEvaluation not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteArtifactEvaluation(@Argument Long id) {
    repository.deleteById(id);
  }

}
