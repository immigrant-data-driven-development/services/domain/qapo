package br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.repositories;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.QualityAssuranceCriterion;
import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories.QualityAssuranceCriterionRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "qualityassurancecriterion", path = "qualityassurancecriterion")
public interface QualityAssuranceCriterionRepositoryWeb extends QualityAssuranceCriterionRepository {

}
