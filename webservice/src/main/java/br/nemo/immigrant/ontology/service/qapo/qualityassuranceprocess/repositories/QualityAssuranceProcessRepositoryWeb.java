package br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.repositories;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.QualityAssuranceProcess;
import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories.QualityAssuranceProcessRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "qualityassuranceprocess", path = "qualityassuranceprocess")
public interface QualityAssuranceProcessRepositoryWeb extends QualityAssuranceProcessRepository {

}
