package br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.controllers;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.AdherenceEvaluation;
import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories.AdherenceEvaluationRepository;
import br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.records.AdherenceEvaluationInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class AdherenceEvaluationController  {

  @Autowired
  AdherenceEvaluationRepository repository;

  @QueryMapping
  public List<AdherenceEvaluation> findAllAdherenceEvaluations() {
    return repository.findAll();
  }

  @QueryMapping
  public AdherenceEvaluation findByIDAdherenceEvaluation(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public AdherenceEvaluation createAdherenceEvaluation(@Argument AdherenceEvaluationInput input) {
    AdherenceEvaluation instance = AdherenceEvaluation.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public AdherenceEvaluation updateAdherenceEvaluation(@Argument Long id, @Argument AdherenceEvaluationInput input) {
    AdherenceEvaluation instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("AdherenceEvaluation not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteAdherenceEvaluation(@Argument Long id) {
    repository.deleteById(id);
  }

}
