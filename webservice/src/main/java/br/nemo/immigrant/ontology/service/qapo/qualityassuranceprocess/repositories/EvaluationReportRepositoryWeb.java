package br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.repositories;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.EvaluationReport;
import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories.EvaluationReportRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "evaluationreport", path = "evaluationreport")
public interface EvaluationReportRepositoryWeb extends EvaluationReportRepository {

}
