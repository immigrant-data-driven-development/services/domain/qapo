package br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.controllers;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.NonComplianceRegister;
import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories.NonComplianceRegisterRepository;
import br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.records.NonComplianceRegisterInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class NonComplianceRegisterController  {

  @Autowired
  NonComplianceRegisterRepository repository;

  @QueryMapping
  public List<NonComplianceRegister> findAllNonComplianceRegisters() {
    return repository.findAll();
  }

  @QueryMapping
  public NonComplianceRegister findByIDNonComplianceRegister(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public NonComplianceRegister createNonComplianceRegister(@Argument NonComplianceRegisterInput input) {
    NonComplianceRegister instance = NonComplianceRegister.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public NonComplianceRegister updateNonComplianceRegister(@Argument Long id, @Argument NonComplianceRegisterInput input) {
    NonComplianceRegister instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("NonComplianceRegister not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteNonComplianceRegister(@Argument Long id) {
    repository.deleteById(id);
  }

}
