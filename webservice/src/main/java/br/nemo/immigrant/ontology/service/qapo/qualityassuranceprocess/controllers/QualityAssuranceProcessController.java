package br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.controllers;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.QualityAssuranceProcess;
import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories.QualityAssuranceProcessRepository;
import br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.records.QualityAssuranceProcessInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class QualityAssuranceProcessController  {

  @Autowired
  QualityAssuranceProcessRepository repository;

  @QueryMapping
  public List<QualityAssuranceProcess> findAllQualityAssuranceProcesss() {
    return repository.findAll();
  }

  @QueryMapping
  public QualityAssuranceProcess findByIDQualityAssuranceProcess(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public QualityAssuranceProcess createQualityAssuranceProcess(@Argument QualityAssuranceProcessInput input) {
    QualityAssuranceProcess instance = QualityAssuranceProcess.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public QualityAssuranceProcess updateQualityAssuranceProcess(@Argument Long id, @Argument QualityAssuranceProcessInput input) {
    QualityAssuranceProcess instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("QualityAssuranceProcess not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteQualityAssuranceProcess(@Argument Long id) {
    repository.deleteById(id);
  }

}
