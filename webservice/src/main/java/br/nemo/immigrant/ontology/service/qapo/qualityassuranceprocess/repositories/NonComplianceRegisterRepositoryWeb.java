package br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.repositories;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.NonComplianceRegister;
import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories.NonComplianceRegisterRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "noncomplianceregister", path = "noncomplianceregister")
public interface NonComplianceRegisterRepositoryWeb extends NonComplianceRegisterRepository {

}
