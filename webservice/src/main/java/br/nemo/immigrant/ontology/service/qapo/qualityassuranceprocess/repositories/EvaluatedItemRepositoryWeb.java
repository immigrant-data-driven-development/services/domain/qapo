package br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.repositories;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.EvaluatedItem;
import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories.EvaluatedItemRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "evaluateditem", path = "evaluateditem")
public interface EvaluatedItemRepositoryWeb extends EvaluatedItemRepository {

}
