package br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.repositories;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.EvaluatedArtifact;
import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories.EvaluatedArtifactRepository;

import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "evaluatedartifact", path = "evaluatedartifact")
public interface EvaluatedArtifactRepositoryWeb extends EvaluatedArtifactRepository {

}
