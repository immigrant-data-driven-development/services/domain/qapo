package br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.controllers;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.QualityAssuranceCriterion;
import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories.QualityAssuranceCriterionRepository;
import br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.records.QualityAssuranceCriterionInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class QualityAssuranceCriterionController  {

  @Autowired
  QualityAssuranceCriterionRepository repository;

  @QueryMapping
  public List<QualityAssuranceCriterion> findAllQualityAssuranceCriterions() {
    return repository.findAll();
  }

  @QueryMapping
  public QualityAssuranceCriterion findByIDQualityAssuranceCriterion(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public QualityAssuranceCriterion createQualityAssuranceCriterion(@Argument QualityAssuranceCriterionInput input) {
    QualityAssuranceCriterion instance = QualityAssuranceCriterion.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public QualityAssuranceCriterion updateQualityAssuranceCriterion(@Argument Long id, @Argument QualityAssuranceCriterionInput input) {
    QualityAssuranceCriterion instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("QualityAssuranceCriterion not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteQualityAssuranceCriterion(@Argument Long id) {
    repository.deleteById(id);
  }

}
