package br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.controllers;

import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.models.EvaluatedItem;
import br.nemo.immigrant.ontology.entity.qapo.qualityassuranceprocess.repositories.EvaluatedItemRepository;
import br.nemo.immigrant.ontology.service.qapo.qualityassuranceprocess.records.EvaluatedItemInput;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.graphql.data.method.annotation.Argument;
import org.springframework.graphql.data.method.annotation.MutationMapping;
import org.springframework.graphql.data.method.annotation.QueryMapping;

import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class EvaluatedItemController  {

  @Autowired
  EvaluatedItemRepository repository;

  @QueryMapping
  public List<EvaluatedItem> findAllEvaluatedItems() {
    return repository.findAll();
  }

  @QueryMapping
  public EvaluatedItem findByIDEvaluatedItem(@Argument Long id) {
    return repository.findById(id).orElse(null);
  }

  /* https://github.com/danvega/graphql-books
  Usar isso para relacao entre os conceitos https://www.danvega.dev/blog/2023/03/20/graphql-mutations/
  */

  @MutationMapping
  public EvaluatedItem createEvaluatedItem(@Argument EvaluatedItemInput input) {
    EvaluatedItem instance = EvaluatedItem.builder().build();

    return repository.save(instance);
  }

  @MutationMapping
  public EvaluatedItem updateEvaluatedItem(@Argument Long id, @Argument EvaluatedItemInput input) {
    EvaluatedItem instance = repository.findById(id).orElse(null);
    if(instance == null) {
        throw new RuntimeException("EvaluatedItem not found");
    }

    repository.save(instance);
    return instance;
  }

  @MutationMapping
  public void deleteEvaluatedItem(@Argument Long id) {
    repository.deleteById(id);
  }

}
